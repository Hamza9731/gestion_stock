from django.contrib import admin
from django.urls import reverse
from .models import Article, Categorie, Couleur, Object, Stock, Taille
from django.utils.safestring import mark_safe

from import_export import resources
from import_export.admin import ImportExportModelAdmin


# Register your models here.
admin.site.register(Categorie)
admin.site.register(Object)
admin.site.register(Taille)

admin.site.register(Couleur)


class StockResource(resources.ModelResource):
    class Meta:
        model = Stock

        exclude = ('barcode', 'code128')


class StockAdmin(ImportExportModelAdmin):
    resource_class = StockResource


admin.site.register(Stock, StockAdmin)


class StockInline(admin.TabularInline):
    model = Stock
    fieldsets = [
        (None, {'fields': ['quantite', 'taille', 'barcode']})
    ]  # list columns
    extra = 1


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    inlines = [StockInline, ]  # list of bookings made by a contact
    search_fields = ['name', 'object__name']
    list_filter = ['object__name', 'object__type__name']

# add couleur

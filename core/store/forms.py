from .models import Article, Couleur, Object
from django.forms import ModelForm
from django import forms


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['name', 'description', 'object', 'couleur',
                  'prix', 'image']


class ObjectForm(ModelForm):
    class Meta:
        model = Object
        fields = ['name', 'type', 'image']


class CouleurForm(ModelForm):
    class Meta:
        model = Couleur
        fields = ['name']

from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic.list import ListView

from .forms import ArticleForm, CouleurForm, ObjectForm

from .models import Couleur, Object, Stock, Article, Categorie
from django.views import generic
from django.contrib.auth.decorators import login_required


class IndexView(generic.ListView):
    template_name = 'store/index.html'
    model = Article

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['vetements'] = Article.objects.filter(
            object__type__name="vetement").order_by('-created_at')[:12]
        context['cosmetiques'] = Article.objects.filter(
            object__type__name="cosmetique").order_by('-created_at')[:12]
        context['bijoux'] = Article.objects.filter(
            object__type__name="bijoux").order_by('-created_at')[:12]
        context['enfants'] = Article.objects.filter(
            object__type__name="enfant").order_by('-created_at')[:12]

        return context


def CategorieView(request):
    categorie = request.GET['categorie']
    context = {}
    liste_categories = [
        categorie__tmp.name for categorie__tmp in Categorie.objects.all()]
    if categorie in liste_categories:
        context["categorie"] = categorie
        context["background"] = Categorie.objects.get(name=categorie).image
        context['articles'] = Article.objects.filter(
            object__type__name=f"{categorie}").order_by('-created_at')

        if("type" in request.GET.keys()):
            context['articles'] = context['articles'].filter(
                object__type__id=request.GET['type'])

        if("couleur" in request.GET.keys()):
            context['articles'] = context['articles'].filter(
                couleur__id=request.GET['couleur'])

        context['objects'] = Object.objects.filter(
            type__name=categorie).order_by('name')

        context['couleurs'] = Couleur.objects.all()
    else:
        return HttpResponseNotFound()

    return render(request, 'store/categorie.html', context)


def articles(request):
    articles = ["<li>{}</li>".format(article.name)
                for article in Article.objects.all()]
    message = """<ul>{}</ul>""".format("\n".join(articles))
    return HttpResponse(message)


def article_detail(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    sizes = Stock.objects.filter(article=article).values_list(
        'taille__name', flat=True).distinct()
    #
    return render(request, 'store/detail.html', {'article': article, 'sizes': sizes})


# <li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>

def sort_size(sizes):

    SORT_ORDER = {"XS": -1, "S": 0, "M": 1, "L": 2, "XL": 3,
                  "2XL": 4, "3XL": 5, "4XL": 6, "5XL": 7, "6XL": 8}
    return sorted(
        sizes, key=lambda elem: SORT_ORDER[elem])


@login_required
def create_article(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('store:index')
    else:
        form = ArticleForm()
    return render(request, 'store/create.html', {'form': form})


@login_required
def create_object(request):
    if request.method == 'POST':
        form = ObjectForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('store:index')
    else:
        form = ObjectForm()
    return render(request, 'store/create.html', {'form': form})


@login_required
def create_couleur(request):
    if request.method == 'POST':
        form = CouleurForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return redirect('store:index')
    else:
        form = CouleurForm()
    return render(request, 'store/create.html', {'form': form})

from django.db import models

from django.db import models
import barcode                      # additional imports
from barcode.writer import ImageWriter
from io import BytesIO
from django.core.files import File
from django.db import models


# Register your models here.


# question = models.ForeignKey(Question, on_delete=models.CASCADE)*


# exemple vetement, chaussure, bijoux
class Categorie(models.Model):
    name = models.CharField(max_length=30)
    image = models.ImageField(
        upload_to='images_categorie/', blank=True, null=True)

    def __str__(self):
        return self.name


class Couleur(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

# exemple XS, ml, cm, 90


class Taille(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


# exemple pull, boucle d'oreille, pantalon


class Object(models.Model):
    name = models.CharField(max_length=30)
    type = models.ForeignKey(Categorie, on_delete=models.CASCADE)
    image = models.ImageField(
        upload_to='images_objects/', blank=True, null=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(
        max_length=300, default="", blank=True, null=True)
    couleur = models.ForeignKey(Couleur, on_delete=models.CASCADE)
    prix = models.FloatField()
    image = models.ImageField(
        upload_to='images_articles/', blank=True, null=True)
    object = models.ForeignKey(Object, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Stock(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    taille = models.ForeignKey(Taille, on_delete=models.CASCADE)
    quantite = models.IntegerField()
    code128 = models.CharField(
        max_length=128, default="", blank=True, null=True)  # code 128
    barcode = models.ImageField(
        upload_to='barcodes/', blank=True, null=True)

    def save(self, *args, **kwargs):          # overriding save()
        COD128 = barcode.get_barcode_class('code128')
        rv = BytesIO()
        code = COD128(
            f'{self.article.object.type.name[0:2]+self.article.object.name[0:2]+self.article.name[0:2]+self.article.couleur.name[0:2]+self.taille.name + str(self.pk)}', writer=ImageWriter()).write(rv)
        self.code128 = code
        self.barcode.save(f'{code}.png', File(rv), save=False)
        return super().save(*args, **kwargs)

    def __str__(self):

        return self.article.name + "_" + self.taille.name

# créole blanche, pull lacoste, pantalon noir

from django.urls import path, re_path

from . import views
app_name = 'store'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('articles', views.articles, name='articles'),
    path('article/<int:article_id>/', views.article_detail, name='detail'),
    path('create_article', views.create_article, name='create_article'),
    path('create_object', views.create_object, name='create_object'),
    path('create_couleur', views.create_couleur, name='create_couleur'),

    re_path(r'^categories/$', views.CategorieView, name='categories'),
    # ex: /polls/5/results/

]

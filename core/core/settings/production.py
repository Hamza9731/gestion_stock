from . import *
from dotenv import load_dotenv


DEBUG = False
SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = ['localhost', '3andlinda.com', 'www.3andlinda.com']

# Static files settings
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
# Extra places for collectstatic to find static files.


# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    f"{BASE_DIR}/store/static",
]
# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/

DB_NAME = os.environ['DB_NAME']
# attention : remplacez par votre nom d'utilisateur
DB_USER = os.environ['DB_USER']
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_HOST = os.environ['DB_HOST']
DB_PORT = os.environ['DB_PORT']
# db
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',  # on utilise l'adaptateur postgresql
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}


MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

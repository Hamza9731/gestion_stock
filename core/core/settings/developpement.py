from . import *
from dotenv import load_dotenv

load_dotenv()

DEBUG = True
SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = ['127.0.0.1', 'localhost',
                 '3andlinda.com', 'www.3andlinda.com']
INTERNAL_IPS = ['127.0.0.1', 'localhost', '3andlinda.com']
# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DB_NAME = os.environ['DB_NAME']
# attention : remplacez par votre nom d'utilisateur
DB_USER = os.environ['DB_USER']
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_HOST = os.environ['DB_HOST']
DB_PORT = os.environ['DB_PORT']
# db
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',  # on utilise l'adaptateur postgresql
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}
# la ou collectstatic va chercher les fichiers statiques
# STATICFILES_DIRS = [
#     f"{BASE_DIR}/staticfiles/static",
# ]
# la ou collectstatic va metter les fichiers statiques
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
# l'url qui va servir les fichiers statiques
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = 'media/'

from django.urls import path, re_path
from django.contrib.auth.views import LogoutView, LoginView

from . import views
app_name = 'accounts'
urlpatterns = [

    path('logout', LogoutView.as_view(
        template_name='accounts/logout.html', extra_context={'connection': 'Merci de votre visite!'}),
        name='logout'),
    path('login', LoginView.as_view(
         template_name='accounts/login.html', redirect_authenticated_user=False, extra_context={'connection': 'Vous voulez vous connecter?'}),
         name='login'),
    #     path('register', views.register_page, name='register'),
    #     path('profile', views.profile_page, name='profile'),
    #     path('profile/edit', views.profile_edit_page, name='profile_edit'),
    #     path('profile/password', views.profile_password_page, name='profile_password'),
    #     path('profile/delete', views.profile_delete_page, name='profile_delete'),
    #     path('profile/delete/confirm', views.profile_delete_confirm_page,
    #          name='profile_delete_confirm'),
    #     path('profile/delete/cancel', views.profile_delete_cancel_page,
    #          name='profile_delete_cancel'),
    #     path('profile/delete/confirm/ok', views.profile_delete_confirm_ok_page,
    #          name='profile_delete_confirm_ok'),
    #     path('profile/delete/confirm/ko', views.profile_delete_confirm_ko_page,
    #          name='profile_delete_confirm_ko'),

]

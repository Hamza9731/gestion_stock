from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):

    VENDEUR = 'VENDEUR'
    GERANT = 'GERANT'

    ROLE_CHOICES = (
        (VENDEUR, 'Vendeur'),
        (GERANT, 'Gerant'),
    )
    profile_photo = models.ImageField(
        verbose_name='Photo de profil', blank=True, null=True)
    role = models.CharField(
        max_length=30, choices=ROLE_CHOICES, verbose_name='Rôle')
